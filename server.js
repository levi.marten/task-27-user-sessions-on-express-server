
const express = require('express');
const app = express();
const path = require('path');
const session = require('express-session');
const { PORT = 3000 } = process.env;

app.use(session({
    secret: 'dwdqwdqdd',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false,
        httpOnly: true,
        maxAge: 360000 * 24
    }
}));

app.get('/', (req, res) => {
    req.session.counter == undefined ? res.sendFile(path.join(__dirname, '/login')) : res.sendFile(path.join(__dirname, '/dashboard'));
});

app.get('/create', (req, res) => {
    req.session;
    req.session.counter++;
});

app.get('/login', (req, res) => {
    res.send('Login screen');
});

app.get('/dashboard', (req, res) => {
    res.send('Dashboard');
});

app.listen(PORT, () => console.log(`Server started on port ${PORT}...`));